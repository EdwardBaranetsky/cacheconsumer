package main

import (
	"log"
	"math/rand"
	"sync"
	"test/CacheConsumer/api"
	"test/CacheConsumer/cache/config"
	"test/CacheConsumer/cache/requestManager"
	"test/CacheConsumer/cache/storage"
)

type Server struct {
	Storage        *storage.Storage
	Config         *config.Config
	RequestManager *requestManager.RequestManager
}

func InitServer() api.CacheServer {
	s := Server{}

	cfg, err := config.InitConfig()
	if err != nil {
		log.Fatalln("[Server] cannot load configs, err:", err)
	}

	s.Config = &cfg

	s.Storage = storage.Init()

	s.RequestManager = requestManager.Init()

	return &s
}

func (s *Server) GetRandomDataStream(empty *api.Empty, stream api.Cache_GetRandomDataStreamServer) error {
	wg := &sync.WaitGroup{}

	wg.Add(int(s.Config.NumberOfRequests))

	for i := uint16(0); i < s.Config.NumberOfRequests; i++ {
		go func() {
			url := s.Config.GetRandomUrl()
			result := s.Storage.Get(url)
			if len(result) == 0 {

				s.RequestManager.Request(url, func(result string, processAsFirst bool) {
					if processAsFirst {
						s.Storage.Set(storage.Data{
							LifeTime: rand.Intn(s.Config.MaxTimeout) + s.Config.MinTimeout,
							Value:    result,
							Key:      url,
						})
					}
					stream.Send(&api.ResponseBody{
						ResponseBody: result,
					})
					wg.Done()
				})

				return
			}

			stream.Send(&api.ResponseBody{
				ResponseBody: result,
			})
			wg.Done()
		}()
	}

	wg.Wait()

	return nil
}
