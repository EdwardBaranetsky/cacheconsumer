package main

import (
	"log"
	"net"

	"google.golang.org/grpc"
	"test/CacheConsumer/api"
)

func main() {
	listener, err := net.Listen("tcp", ":7777")
	if err != nil {
		log.Fatalln("cannot listen tcp, err:", err)
	}

	serverInstance := InitServer()

	grpcServer := grpc.NewServer()

	api.RegisterCacheServer(grpcServer, serverInstance)

	if err = grpcServer.Serve(listener); err != nil {
		log.Println("grpc server err:", err)
	}
}
