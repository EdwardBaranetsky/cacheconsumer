package storage

import (
	"github.com/go-redis/redis"
	"log"
	"time"
)

type Data struct {
	Key      string
	Value    string
	LifeTime int
}

type Storage struct {
	redisClient *redis.Client
}

func Init() *Storage {
	storage := Storage{}

	storage.redisClient = redis.NewClient(&redis.Options{
		Addr:     "127.0.0.1:6379",
		Password: "",
		DB:       0,
	})

	_, err := storage.redisClient.Ping().Result()
	if err != nil {
		log.Fatalln("[RedisClient] ping err:", err)
	}

	return &storage
}

func (s *Storage) Set(data Data) {
	s.redisClient.Set(data.Key, data.Value, time.Duration(data.LifeTime)*time.Second)
}

func (s Storage) Get(key string) string {
	return s.redisClient.Get(key).Val()
}
