package requestManager

import (
	"golang.org/x/net/html/charset"
	"io/ioutil"
	"net/http"
	"sync"
)

type Sender func(body string, processAsFirst bool)

type RequestManager struct {
	commands map[string][]Sender
	lock     sync.RWMutex
	done     uint64
	started  uint64
}

func Init() *RequestManager {
	return &RequestManager{
		commands: make(map[string][]Sender),
	}
}

func (rm *RequestManager) addSender(url string, sender Sender) (isNewUrl bool) {
	rm.lock.Lock()
	defer rm.lock.Unlock()

	rm.commands[url] = append(rm.commands[url], sender)

	return len(rm.commands[url]) == 1
}

func (rm *RequestManager) processCommands(url string, result string) {
	rm.lock.Lock()
	for index, sender := range rm.commands[url] {
		sender(result, index == 0)
	}

	delete(rm.commands, url)
	rm.lock.Unlock()
}

func (rm *RequestManager) Request(url string, sender Sender) {

	if rm.addSender(url, sender) == false {
		return
	}

	var result string

	resp, err := http.Get(url)

	if resp != nil && resp.Body != nil {
		defer resp.Body.Close()
	}
	if err != nil {
		result = err.Error()
	} else {
		utf8, err := charset.NewReader(resp.Body, resp.Header.Get("Content-Type"))
		if err != nil {
			result = err.Error()
		}

		respBytes, err := ioutil.ReadAll(utf8)
		if err != nil {
			result = err.Error()
		} else {
			result = string(respBytes)
		}
	}

	rm.processCommands(url, result)
}
