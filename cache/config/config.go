package config

import (
	"gopkg.in/yaml.v2"
	"io/ioutil"
	"math/rand"
	"sync"
)

const ymlConfigPath = "./cache/config/config.yml"

type Config struct {
	URLs             []string `yaml:"URLs,flow"`
	MinTimeout       int      `yaml:"MinTimeout"`
	MaxTimeout       int      `yaml:"MaxTimeout"`
	NumberOfRequests uint16   `yaml:"NumberOfRequests"`
	lock             sync.RWMutex
}

func InitConfig() (config Config, err error) {

	fileBytes, err := ioutil.ReadFile(ymlConfigPath)
	if err != nil {
		return
	}

	err = yaml.Unmarshal(fileBytes, &config)

	return
}

func (c Config) GetRandomUrl() string {
	c.lock.RLock()
	defer c.lock.RUnlock()

	return c.URLs[rand.Intn(len(c.URLs))]
}
