package main

import (
	"context"
	"google.golang.org/grpc"
	"io"
	"log"
	"os"
	"strconv"
	"sync"
	"test/CacheConsumer/api"
)

func main() {

	conn, err := grpc.Dial("127.0.0.1:7777", grpc.WithInsecure())
	if err != nil {
		log.Fatalln("grpc dial err: ", err)
	}

	client := api.NewCacheClient(conn)

	wg := sync.WaitGroup{}

	wg.Add(1000)

	for i := 0; i < 1000; i++ {
		go func(number int) {
			defer wg.Done()

			streamClient, err := client.GetRandomDataStream(context.Background(), &api.Empty{})
			if err != nil {
				log.Println("stream client err:", err)
				return
			}

			for {
				respBody, err := streamClient.Recv()
				if err == io.EOF {
					log.Println("stream closed")
					break
				} else if err != nil {
					log.Println("error happed, err:", err)
					break
				}

				fileHandler, err := os.OpenFile("./result.txt", os.O_WRONLY|os.O_CREATE|os.O_APPEND, 0666)
				if err != nil {
					log.Println("cannot open file")
					continue
				}

				fileHandler.WriteString(strconv.Itoa(number) + " : " + strconv.Itoa(len(respBody.ResponseBody)) + "\n")
				fileHandler.Close()
			}
		}(i)
	}

	wg.Wait()
}
