clean:
	docker stop redis
	docker rm redis

envrun:
	docker start redis || docker run --name redis -p 6379:6379 -d redis

runCache:
	go run cache/*.go

runConsumer:
	go run consumer/*.go
